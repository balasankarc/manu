#!/usr/bin/env python

import calendar
import json
import platform
import urllib2
from datetime import datetime, timedelta

from lxml import etree


def find_closest_month(list_of_months):
    current_month = datetime.now()

    def func(x):
        d = x
        delta = d - current_month if d > current_month else timedelta.max
        return delta
    result = min(list_of_months, key=func)
    return result


def find_platform():
    os = platform.system()
    if os == 'Windows':
        version = platform.release()
        return os, version
    else:
        return 'Windows', '7'  # Dummy for GNU/Linux


def get_available_months():
    os, version = find_platform()
    class_name = "%s-%s" % (os.lower(), version.lower())
    print class_name
    headers = {'User-Agent': 'Mozilla/5.0'}
    url = 'https://www.av-test.org/en/antivirus/home-windows/%s' % class_name
    req = urllib2.Request(url, None, headers)
    page = urllib2.urlopen(req)
    sourcecontent = page.read()
    tree = etree.HTML(sourcecontent)
    listtags = tree.xpath(
        '//div[contains(@class, "platform-%s")]' % class_name)
    months = listtags[0].xpath(
        '../div[contains(@class, "rl-series-list")]/ul/li/a/text()')
    months = [datetime.strptime(x.strip(), "%B %Y") for x in months]
    return class_name, months

class_name, available_months = get_available_months()
closest_month = find_closest_month(available_months)
url_suffix = datetime.strftime(closest_month, "%B-%Y").lower()
urls = {
    'windows-7': 'http://www.av-test.org/en/antivirus/home-windows/windows-7',
    'windows-8': 'http://www.av-test.org/en/antivirus/home-windows/windows-8',
    'windows-10': 'http://www.av-test.org/en/antivirus/home-windows/windows-10'
}
mainurl = urls[class_name] + '/' + url_suffix


headers = {'User-Agent': 'Mozilla/5.0'}
req = urllib2.Request(mainurl, None, headers)
page = urllib2.urlopen(req)
sourcecontent = page.read()
tree = etree.HTML(sourcecontent)
result = {}
avtags = tree.xpath('//tr[contains(@class, "rl-reportrow")]')
for avtag in avtags:
    namet = avtag.xpath(
        'td[contains(@class, "rl-report-name")]/span/strong/text()')
    name = namet[0]
    linkt = avtag.xpath('td[contains(@class, "rl-report-details")]/a/@href')
    link = 'https://www.av-test.org/' + linkt[0]
    result[name] = {'link': link}
for name, items in result.items():
    print name
    link = items['link']
    avreq = urllib2.Request(link, None, headers)
    avpage = urllib2.urlopen(avreq)
    avsourcecontent = avpage.read()
    avtree = etree.HTML(avsourcecontent)
    paramtag = avtree.xpath('//div[contains(@class, "rl-subtest-res")]')
    parameters = []
    scores = []
    for tag in paramtag[1::2]:
        scorenametag = tag.xpath('text()')
        parameter = scorenametag[0].strip()
        scoretag = tag.xpath('span/text()')
        score = scoretag[0].strip()
        result[name][parameter] = score
        print "\t", parameter, " : ", score
print json.dumps(result, indent=4)
