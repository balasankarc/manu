#!/usr/bin/env python
# -*- coding: utf-8 -*-


import calendar
import json
import os
import platform
import sys
import urllib2
from datetime import datetime, timedelta

from lxml import etree
from PySide.QtCore import *
from PySide.QtGui import *


class MyDialog(QDialog):

    def __init__(self, parent=None):
        super(MyDialog, self).__init__(parent)
        self.setWindowTitle('SecTestAuto')
        self.resize(600, 300)
        self.parent = parent

        pmap1 = QPixmap('protection.png').scaled(100, 100)
        pmap2 = QPixmap('performance.png').scaled(100, 100)
        pmap3 = QPixmap('usability.png').scaled(100, 100)
        pmap4 = QPixmap('repair.png').scaled(100, 100)

        self.avname = QLabel("Sample")
        self.avname.setAlignment(Qt.AlignCenter)
        font = QFont("FreeMono", 25)
        font.setBold(True)
        self.avname.setFont(font)

        self.label1 = QLabel()
        self.label1.setAlignment(Qt.AlignCenter)
        self.label11 = QLabel("Protection")
        self.label11.setAlignment(Qt.AlignCenter)
        self.label1.setPixmap(pmap1)
        self.label2 = QLabel()
        self.label2.setAlignment(Qt.AlignCenter)
        self.label2.setPixmap(pmap2)
        self.label22 = QLabel("Performance")
        self.label22.setAlignment(Qt.AlignCenter)
        self.label3 = QLabel()
        self.label3.setAlignment(Qt.AlignCenter)
        self.label3.setPixmap(pmap3)
        self.label33 = QLabel("Usability")
        self.label33.setAlignment(Qt.AlignCenter)
        self.label4 = QLabel()
        self.label4.setAlignment(Qt.AlignCenter)
        self.label4.setPixmap(pmap4)
        self.label44 = QLabel("Repair")
        self.label44.setAlignment(Qt.AlignCenter)
        font = QFont("FreeMono", 13)
        font.setBold(True)
        self.label11.setFont(font)
        self.label22.setFont(font)
        self.label33.setFont(font)
        self.label44.setFont(font)

        self.progress1 = QProgressBar(self)
        self.progress2 = QProgressBar(self)
        self.progress3 = QProgressBar(self)
        self.progress4 = QProgressBar(self)

        self.label_protection = QLabel("")
        self.label_performance = QLabel("")
        self.label_usability = QLabel("")
        self.label_repair = QLabel("")
        font = QFont("FreeMono", 20)
        font.setBold(True)
        self.label_protection.setFont(font)
        self.label_performance.setFont(font)
        self.label_usability.setFont(font)
        self.label_repair.setFont(font)
        self.label_protection.setAlignment(Qt.AlignCenter)
        self.label_performance.setAlignment(Qt.AlignCenter)
        self.label_usability.setAlignment(Qt.AlignCenter)
        self.label_repair.setAlignment(Qt.AlignCenter)

        filename = os.path.basename(self.parent.selected_filename)
        self.selected_avname = self.check(filename)
        self.avname.setText(self.selected_avname)

        grid = QGridLayout()
        grid.addWidget(self.avname, 0, 0, 1, 10)
        grid.addWidget(self.label1, 1, 0)
        grid.addWidget(self.label2, 1, 2)
        grid.addWidget(self.label3, 1, 4)
        grid.addWidget(self.label4, 1, 6)
        grid.addWidget(self.label11, 2, 0)
        grid.addWidget(self.label22, 2, 2)
        grid.addWidget(self.label33, 2, 4)
        grid.addWidget(self.label44, 2, 6)
        grid.addWidget(self.label_protection, 3, 0)
        grid.addWidget(self.label_performance, 3, 2)
        grid.addWidget(self.label_usability, 3, 4)
        grid.addWidget(self.label_repair, 3, 6)
        grid.addWidget(self.progress1, 4, 0)
        grid.addWidget(self.progress2, 4, 2)
        grid.addWidget(self.progress3, 4, 4)
        grid.addWidget(self.progress4, 4, 6)

        self.verticalLayout = QVBoxLayout(self)
        self.verticalLayout.addLayout(grid)

        self.buttonBox = QDialogButtonBox(self)
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(
            QDialogButtonBox.Ok | QDialogButtonBox.Close)
        self.buttonBox.button(QDialogButtonBox.Ok).setText("Save Log")
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)
        self.buttonBox.button(QDialogButtonBox.Close).setEnabled(False)
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.close)
        self.verticalLayout.addStretch()
        self.verticalLayout.addWidget(self.buttonBox)
        if self.selected_avname:
            self.status = True
            self.show()
            self.process()
        else:
            mbox = QMessageBox(self.parent)
            mbox.setIcon(QMessageBox.Critical)
            mbox.setText("Settings not found")
            mbox.setStandardButtons(QMessageBox.Ok)
            reply = mbox.exec_()
            self.status = False

    def process(self):
        filepath = os.path.abspath(os.path.join('data', 'database.json'))
        database_file = open(filepath)
        result = json.loads(database_file.read())
        database_file.close()
        self.result_av = result[self.selected_avname]
        self.result_av.pop('link', None)
        self.result_av['Antivirus Name'] = self.selected_avname

        performance_raw = self.result_av['Performance Score']
        performance_string = performance_raw[:performance_raw.index('/')]
        performance = float(performance_string)

        protection_raw = self.result_av['Protection Score']
        protection_string = protection_raw[:protection_raw.index('/')]
        protection = float(protection_string)

        usability_raw = self.result_av['Usability Score']
        usability_string = usability_raw[:usability_raw.index('/')]
        usability = float(usability_string)

        average = (performance + protection + usability) / 3.0
        repair = round(average, 2)

        self.completed = 0
        while self.completed < 100:
            self.completed += 0.00005
            self.progress1.setValue(self.completed)
        self.label_protection.setText(str(performance))

        self.completed = 0
        while self.completed < 100:
            self.completed += 0.0000055
            self.progress2.setValue(self.completed)
        self.label_performance.setText(str(protection))

        self.completed = 0
        while self.completed < 100:
            self.completed += 0.000025
            self.progress3.setValue(self.completed)
        self.label_usability.setText(str(usability))

        self.completed = 0
        while self.completed < 100:
            self.completed += 0.000025
            self.progress4.setValue(self.completed)
        self.label_repair.setText(str(repair))

        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(True)
        self.buttonBox.button(QDialogButtonBox.Close).setEnabled(True)

    def accept(self):
        current_date = datetime.now().__str__().replace(':', '_')
        current_date = current_date[:current_date.index('.')]
        filename = current_date + ".log"
        filepath = os.path.abspath(os.path.join('log', filename))
        f = open(filepath, 'w')
        f.write(json.dumps(self.result_av, indent=4))
        f.close()
        mbox = QMessageBox(self)
        mbox.setIcon(QMessageBox.Information)
        mbox.setText("Log saved : " + filepath)
        mbox.setStandardButtons(QMessageBox.Ok)
        reply = mbox.exec_()

    def check(self, filename):
        name_map = [
            [('gdata', 'gd'), u'G Data'],
            [('bitdefender', 'bd'), 'Bitdefender'],
            [('mcafee', 'mc'), 'McAfee'],
            [('norton', 'na'), 'Norton'],
            [('qihoo', 'gi'), 'Qihoo 360'],
            [('k7', 'k7anti'), 'K7 Computing'],
            [('avast', 'ava'), 'Avast'],
            [('comodo', 'com'), 'Comodo'],
            [('fsecure', 'fs'), 'F-Secure'],
            [('ahnlab', 'ah'), 'AhnLab'],
            [('kaspersky', 'kas'), 'Kaspersky Lab'],
            [('emisoft', 'emi'), 'Emsisoft'],
            [('trend', 'tre'), 'Trend Micro'],
            [('avira', 'avir'), 'Avira'],
            [('threattrack', 'thr'), 'ThreatTrack'],
            [('bullguard', 'bg'), 'BullGuard'],
            [('microsoft', 'ms'), 'Microsoft'],
            [('microworld', 'mw'), 'MicroWorld'],
            [('eset', 'esetanti'), 'ESET'],
            [('quickheal', 'quick', 'qh'), 'Quick Heal'],
            [('pandasecurity', 'panda'), 'Panda Security'],
            [('avg', 'avganti'), 'AVG']
        ]
        for item in name_map:
            for regex in item[0]:
                if regex in filename.lower():
                    return item[1]
        return None


class GUI(QMainWindow):

    def __init__(self):
        super(GUI, self).__init__()
        self.initUI()

    def initUI(self):

        self.setGeometry(320, 320, 400, 300)
        self.setWindowTitle('SecTestAuto')
        self.widget = QWidget(self)
        self.setWindowIcon(QIcon('web.png'))
        layout = QFormLayout()

        exitAction = QAction('&Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(qApp.quit)
        aboutAction = QAction('&About', self)
        aboutAction.setShortcut('Ctrl+A')
        aboutAction.setStatusTip('About application')
        aboutAction.triggered.connect(self.showAbout)
        updateAction = QAction('&Update Database', self)
        updateAction.setShortcut('Ctrl+U')
        updateAction.setStatusTip('Update Database')
        updateAction.triggered.connect(self.update_database)

        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        helpMenu = menubar.addMenu('&Help')
        fileMenu.addAction(exitAction)
        fileMenu.addAction(updateAction)
        helpMenu.addAction(aboutAction)

        grid = QGridLayout()
        grid.setSpacing(1)

        self.label1 = QLabel()
        font = QFont("FreeMono", 20)
        font.setBold(True)
        self.label1.setFont(font)
        self.label1.setText("Select the AV Software")
        self.label1.setMaximumHeight(50)
        self.label1.setAlignment(Qt.AlignCenter)

        self.filename = QLineEdit()
        self.filebtn = QPushButton("Select File")
        self.filebtn.clicked.connect(self.filebtnclick)
        self.gobtn = QPushButton("Start Test")
        self.gobtn.clicked.connect(self.start)
        self.gobtn.setEnabled(False)

        self.progress = QProgressBar(self)

        grid.addWidget(self.label1, 0, 0, 1, 2)
        grid.addWidget(self.filename, 1, 0)
        grid.addWidget(self.filebtn, 1, 1)
        grid.addWidget(self.gobtn, 2, 0, 1, 2)
        grid.addWidget(self.progress, 3, 0, 1, 2)

        self.widget.setLayout(grid)
        self.setCentralWidget(self.widget)
        self.setLayout(layout)
        self.show()

    def filebtnclick(self):
        dialog = QFileDialog(self)
        if dialog.exec_():
            self.selected_filename = dialog.selectedFiles()[0]
            self.filename.setText(self.selected_filename)
            self.gobtn.setEnabled(True)

    def showAbout(self):
        mbox = QMessageBox(self)
        mbox.setWindowTitle("SecTestAuto - About")
        mbox.setTextFormat(Qt.TextFormat.RichText)
        mbox.setIcon(QMessageBox.Information)
        mbox.setInformativeText("<p align='justify'>Security Testing - Automated. This tool is intended to automate the process of inspection, analysis and statistics generation of Antivirus softwares with regard to their different functional metric like protection, performance, usability etc. It generates a log of the measured values of an AV software in JSON format.</p><br /><p align='center'>Developed by Manusankar C</p>")
        mbox.setText("\t\tAbout - SecTestAuto v1.0\t\t")
        mbox.setStandardButtons(QMessageBox.Ok)
        reply = mbox.exec_()

    def start(self):
        self.completed = 0
        while self.completed < 100:
            self.completed += 0.00001
            self.progress.setValue(self.completed)
        filename = self.filename.text()
        a = MyDialog(parent=self)
        if a.status:
            a.exec_()

    def find_closest_month(self, list_of_months):
        current_month = datetime.now()
        return min(list_of_months, key=lambda x: abs(x - current_month))

    def find_platform(self):
        os = platform.system()
        if os == 'Windows':
            version = platform.release()
            return os, version
        else:
            return 'Windows', '7'  # Dummy for GNU/Linux

    def get_available_months(self):
        month_map = {'January': 1, 'February': 2, 'March': 3, 'April': 4,
                     'May': 5, 'June': 6, 'July': 7, 'August': 8,
                     'September': 9, 'October': 10, 'November': 11,
                     'December': 12}
        os, version = self.find_platform()
        class_name = "%s-%s" % (os.lower(), version.lower())
        headers = {'User-Agent': 'Mozilla/5.0'}
        url = 'https://www.av-test.org/en/antivirus/home-windows/%s' \
            % class_name
        req = urllib2.Request(url, None, headers)
        page = urllib2.urlopen(req)
        sourcecontent = page.read()
        tree = etree.HTML(sourcecontent)
        listtags = tree.xpath(
            '//div[contains(@class, "platform-%s")]' % class_name)
        months = listtags[0].xpath(
            '../div[contains(@class, "rl-series-list")]/ul/li/a/text()')
        month = []
        for x in months:
            date = x.strip()
            month_name = date[:date.index(' ')]
            year = int(date[date.index(' ') + 1:])
            month_number = month_map[month_name]
            item = datetime.strptime("%d/%d" % (month_number, year), "%m/%Y")
            month.append(item)
        return class_name, month

    def update_database(self):
        self.progress.setValue(2)
        class_name, available_months = self.get_available_months()
        closest_month = self.find_closest_month(available_months)
        url_suffix = datetime.strftime(closest_month, "%B-%Y").lower()
        prefix = 'http://www.av-test.org/en/antivirus/'
        urls = {
            'windows-7': 'home-windows/windows-7',
            'windows-8': 'home-windows/windows-8',
            'windows-10': 'home-windows/windows-10'
        }
        mainurl = prefix + urls[class_name] + '/' + url_suffix
        headers = {'User-Agent': 'Mozilla/5.0'}
        req = urllib2.Request(mainurl, None, headers)
        page = urllib2.urlopen(req)
        sourcecontent = page.read()
        tree = etree.HTML(sourcecontent)
        result = {}
        avtags = tree.xpath('//tr[contains(@class, "rl-reportrow")]')
        for avtag in avtags:
            namet = avtag.xpath(
                'td[contains(@class, "rl-report-name")]/span/strong/text()')
            name = namet[0]
            linkt = avtag.xpath(
                'td[contains(@class, "rl-report-details")]/a/@href')
            link = 'https://www.av-test.org/' + linkt[0]
            result[name] = {'link': link}
        count = len(result)
        unit = 100.0 / count
        for name, items in result.items():
            link = items['link']
            avreq = urllib2.Request(link, None, headers)
            avpage = urllib2.urlopen(avreq)
            avsourcecontent = avpage.read()
            avtree = etree.HTML(avsourcecontent)
            paramtag = avtree.xpath(
                '//div[contains(@class, "rl-subtest-res")]')
            parameters = []
            scores = []
            for tag in paramtag[1::2]:
                scorenametag = tag.xpath('text()')
                parameter = scorenametag[0].strip()
                scoretag = tag.xpath('span/text()')
                score = scoretag[0].strip()
                result[name][parameter] = score
            current = self.progress.value()
            self.progress.setValue(current + unit)
        self.progress.setValue(100)
        filepath = os.path.abspath(os.path.join('data', 'database.json'))
        output_file = open(filepath, 'w')
        output_file.write(json.dumps(result, indent=4))
        output_file.close()
        mbox = QMessageBox(self)
        mbox.setIcon(QMessageBox.Information)
        mbox.setText("Database Updated")
        mbox.setStandardButtons(QMessageBox.Ok)
        reply = mbox.exec_()
        self.progress.setValue(0)


def main():

    app = QApplication(sys.argv)
    ex = GUI()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
